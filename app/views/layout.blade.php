<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Valoran Portal</title>
    <meta name="generator" content="Page Meta Tag">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    {{ HTML::style('css/style.min.css') }}
    {{ HTML::style('css/main.css') }}
    <!--Scripts Here-->
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    {{ HTML::script('js/custom.js') }}

</head>

<body>
<div class="wrapper">
    @yield('content')
</div>
</body>
</html>
